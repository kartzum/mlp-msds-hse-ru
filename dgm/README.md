## DGM

### Description

It is implementation
[Probabilistic Forecasting of Sensory Data With Generative Adversarial Networks – ForGAN](https://ieeexplore.ieee.org/abstract/document/8717640)
. Based on [forgan](https://git.opendfki.de/koochali/forgan).

ForGAN is one step ahead probabilistic forecasting model. It utilizes the power of the conditional generative 
adversarial network to learn the probability distribution of future values given the previous values.

[There is architecture.](https://git.opendfki.de/koochali/forgan)

In this implementation ForGAN is trained for predicting financial times series using real data from a stock.

### References

* [Probabilistic Forecasting of Sensory Data With Generative Adversarial Networks – ForGAN](https://ieeexplore.ieee.org/abstract/document/8717640)
* https://medium.com/swlh/stock-price-prediction-with-pytorch-37f52ae84632
* [Quant GANs: Deep Generation of Financial Time Series](https://arxiv.org/abs/1907.06673)
* [Conditional Sig-Wasserstein GANs for Time Series Generation](https://arxiv.org/pdf/2006.05421.pdf)

### Metric and results

The performance of ForGAN and other models using RMSE.

### Results
|  | Baseline | ForGAN |
| ------ | ------ | ------ |
| AMZN_2006-01-01_to_2018-01-01.csv  | 444.7578 | 325.9591 |

Results show that ForGAN works better but using model in real word have to improvements. 

### Main part & Code

* mp_solution.py - code
* mp_solution_an.ipynb - analysis and demonstration

### How to run

There are examples for running code.

#### Train

```
python3 mp_solution.py -ff=train
```

#### Predict

```
python3 mp_solution.py -ff=predict
```

### Data

* [DJIA 30 Stock Time Series](https://www.kaggle.com/szrlee/stock-time-series-20050101-to-20171231)

### Environment

See: requirements.txt
