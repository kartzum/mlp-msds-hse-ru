## VAE

### Papers

* [Gaussian Process Prior Variational Autoencoders](https://arxiv.org/abs/1810.11738)

### Main part

* exp/hse_ml_pr_exp_v1.ipynb
* paper/Gaussian_Process_Prior_Variational_Autoencoders.pdf

### Works

* w1
* w2

### Implementation_of_VAE_in_PyTorch

* https://arxiv.org/pdf/1906.02691.pdf

### Resources

* https://github.com/fpcasale/GPPVAE
* [Прикладные задачи анализа данных](http://wiki.cs.hse.ru/%D0%9F%D1%80%D0%B8%D0%BA%D0%BB%D0%B0%D0%B4%D0%BD%D1%8B%D0%B5_%D0%B7%D0%B0%D0%B4%D0%B0%D1%87%D0%B8_%D0%B0%D0%BD%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0_%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D1%85)
* [Real-world Low-light Image Enhancement Using Variational Autoencoders](https://lup.lub.lu.se/luur/download?func=downloadFile&recordOId=9030626&fileOId=9030815)  
* [Variational Inference in Sparse Gaussian Processes and Variational Auto-Encoders](https://joo.st/blog/variational_inference_sgp_from_vae.html)
* [Robust Variational Autoencoder](https://arxiv.org/pdf/1905.09961.pdf) 
* https://arxiv.org/pdf/1312.6114.pdf
* https://stats.stackexchange.com/questions/60299/how-do-you-compare-two-gaussian-processes
* https://github.com/kglore/llnet_color/blob/master/library.py
* http://www.frccsc.ru/sites/default/files/docs/ds/002-073-05/diss/26-bahteev/ds05-26-bahteev_main.pdf?28
* [Deep_Learning_Cookbook_Practical_Recipes.indb](http://www.williamspublishing.com/PDF/978-5-907144-50-7/part.pdf)
* https://github.com/KseniyaLem/vae-lstm
* [Variational autoencoders.](https://www.jeremyjordan.me/variational-autoencoders/)
* [Auto-Encoding Variational Bayes](https://arxiv.org/pdf/1312.6114.pdf)
* https://medium.com/analytics-vidhya/generative-modelling-using-variational-autoencoders-vae-and-beta-vaes-81a56ef0bc9f
* [An Introduction to Variational Autoencoders](https://arxiv.org/pdf/1906.02691.pdf)
* http://adamlineberry.ai/vae-series/vae-code-experiments
* https://github.com/Saswatm123/MMD-VAE
* [Understanding Variational Autoencoders (VAEs)](https://towardsdatascience.com/understanding-variational-autoencoders-vaes-f70510919f73)
* https://chriscremer.bitbucket.io/other/phd/Cremer_Chris_A_202011_PhD_thesis_withlogo.pdf
* https://hal.inria.fr/hal-02154181/document
* https://jaan.io/what-is-variational-autoencoder-vae-tutorial/ https://github.com/altosaar/variational-autoencoder/blob/master/train_variational_autoencoder_pytorch.py
* https://github.com/ac-alpha/VAEs-using-Pytorch
* https://medium.com/@sikdar_sandip/implementing-a-variational-autoencoder-vae-in-pytorch-4029a819ccb6
* [Architectures](https://ml-cheatsheet.readthedocs.io/en/latest/architectures.html)
* https://wiseodd.github.io/techblog/2017/01/24/vae-pytorch/
* https://www.topbots.com/variational-autoencoders-explained/
* https://jaan.io/what-is-variational-autoencoder-vae-tutorial/
* https://github.com/altosaar/variational-autoencoder/blob/master/train_variational_autoencoder_pytorch.py
* https://github.com/chriscremer/Other_Code/blob/master/VAE/pytorch/vae.py
* https://github.com/geyang/variational_autoencoder_pytorch
* http://adamlineberry.ai/vae-series/vae-code-experiments
* https://ermongroup.github.io/cs228-notes/extras/vae/
* https://towardsdatascience.com/variational-autoencoder-demystified-with-pytorch-implementation-3a06bee395ed
* https://sites.google.com/andrew.cmu.edu/tarrlab/resources/tarrlab-stimuli?authuser=0#h.u2lsuc5ur5gt
* [How is KL-divergence in pytorch code related to the formula?](https://stackoverflow.com/questions/61597340/how-is-kl-divergence-in-pytorch-code-related-to-the-formula)
* https://github.com/pytorch/examples/blob/master/vae/main.py
* http://summer-school-gan.compute.dtu.dk/slides_marco.pdf
* [Tutorial: Abdominal CT Image Synthesis with Variational Autoencoders using PyTorch](https://medium.com/miccai-educational-initiative/tutorial-abdominal-ct-image-synthesis-with-variational-autoencoders-using-pytorch-933c29bb1c90)
* https://github.com/berradamm/AE-VAE/blob/master/FaceGenerationVAE.ipynb
* [Training a Variational Auto-Encoder](https://colab.research.google.com/github/pytorchbearer/torchbearer/blob/master/docs/_static/notebooks/vae.ipynb)
* [IMPORTANCE WEIGHTED AUTOENCODERS](https://arxiv.org/pdf/1509.00519.pdf)
* [face_place](https://sites.google.com/andrew.cmu.edu/tarrlab/resources/tarrlab-stimuli?authuser=0#h.u2lsuc5ur5gt)
* [face_place](https://tdlc.ucsd.edu/tdlc2/TDLC_Toolkit.php)
* [Distance assessment and analysis of high-dimensional samples using variational autoencoders](https://www.sciencedirect.com/science/article/pii/S0020025520306538)
* [An Introduction to Deep Generative Modeling](https://arxiv.org/pdf/2103.05180.pdf)
* https://dfdazac.github.io/01-vae.html
* [Auto-Encoding Variational Bayes](https://arxiv.org/pdf/1312.6114.pdf)
* https://github.com/yunjey/pytorch-tutorial
* https://hal.archives-ouvertes.fr/hal-03126419/document
* https://github.com/metodj/FGP-VAE
* https://www.researchgate.net/publication/345971554_Factorized_Gaussian_Process_Variational_Autoencoders
* http://proceedings.mlr.press/v118/pearce20a/pearce20a.pdf
* http://dustintran.com/papers/TranRanganathBlei2016.pdf
* https://www.cambridge.org/core/services/aop-cambridge-core/content/view/CBE6D9D6A444412C860FBEEA7F22E620/S2048770319000258a.pdf/learning_priors_for_adversarial_autoencoders.pdf
* https://colab.research.google.com/github/krasserm/bayesian-machine-learning/blob/master/latent_variable_models_part_2.ipynb
* https://github.com/AndrewSpano/Disentangled_Variational_Autoencoder
* https://www.ritchievink.com/blog/2019/09/16/variational-inference-from-scratch/
* https://medium.com/@wuga/generate-anime-character-with-variational-auto-encoder-81e3134d1439
* https://www.kaggle.com/averkij/variational-autoencoder-and-faces-generation


### vae-tutorials

* [what-is-variational-autoencoder-vae-tutorial](https://jaan.io/what-is-variational-autoencoder-vae-tutorial/)

### Theory

* https://en.m.wikipedia.org/wiki/Multivariate_normal_distribution

### sources

* https://colab.research.google.com/drive/1_yGmk8ahWhDs23U4mpplBFa-39fsEJoT?usp=sharing#scrollTo=a3_ABiO0F3jM

### pytorch

* [pytorch-tutorial](https://www.kaggle.com/getting-started/123904)
* [pytorch-tutorial](https://bhashkarkunal.medium.com/pytorch-tutorial-from-basic-to-advance-level-a-numpy-replacement-and-deep-learning-framework-that-a3c8dcf9a9d4)

